import { Column, Entity, OneToMany, PrimaryColumn } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Role {
    @PrimaryColumn()
    id: number;

    @Column()
    nama: string;

    @OneToMany(() => User, (user) => user.role)
    user: User[]
}