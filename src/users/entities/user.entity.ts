import { Column, DeleteDateColumn, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Role } from "./role.entity";

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    nama: string;

    @Column({ unique: true })
    email: string;

    @Column()
    password: string;

    @Column()
    salt: string;

    @ManyToOne(() => Role, (role) => role.user)
    role: Role;

    @DeleteDateColumn({
        type: 'timestamp with time zone',
        nullable: false,
    })
    deletedAt: Date;
}