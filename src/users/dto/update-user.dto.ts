import { IsEmail, IsNotEmpty } from "class-validator";

export class UpdateUserDto {
    @IsNotEmpty()
    nama: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;
}