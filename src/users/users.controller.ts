import { Body, Controller, Delete, Get, HttpStatus, Param, ParseUUIDPipe, Put } from '@nestjs/common';
import { UpdatePasswordDto } from './dto/update-password.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Get('get-all-users')
    async findAll() {
        const [ data, count ] = await this.usersService.findAll();
        return {
            data,
            count,
            statusCode: HttpStatus.OK,
            message: 'success',
        }
    }

    @Get('get-user/:id')
    async findOne(@Param('id', ParseUUIDPipe) id: string) {
        return {
            data: await this.usersService.findOne(id),
            statusCode: HttpStatus.OK,
            message: 'success',
        }
    }

    @Put('edit-user/:id')
    async update(
        @Param('id', ParseUUIDPipe) id: string,
        @Body() request: UpdateUserDto,
    ) {
        return {
            data: await this.usersService.update(id, request),
            statusCode: HttpStatus.OK,
            message: 'success',
        }
    }

    @Put('edit-password/:id')
    async updatePassword(
        @Param('id', ParseUUIDPipe) id: string,
        @Body() request: UpdatePasswordDto,
    ) {
        return {
            data: await this.usersService.updatePassword(id, request),
            statusCode: HttpStatus.OK,
            message: 'success',
        }
    }

    @Delete('delete-user/:id')
    async delete(@Param('id', ParseUUIDPipe) id: string) {
        await this.usersService.remove(id);

        return {
            statusCode: HttpStatus.OK,
            message: 'success'
        }
    }
}
