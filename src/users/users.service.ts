import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { generateString, InjectRepository } from '@nestjs/typeorm';
import { hashPassword } from 'helper/hash_password';
import { EntityNotFoundError, Repository } from 'typeorm';
import { UpdatePasswordDto } from './dto/update-password.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Role } from './entities/role.entity';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Role)
        private roleRepository: Repository<Role>,
    ) {}

    async findAll() {
        return this.userRepository.findAndCount({
            relations: {
                role: true
            }
        })
    }

    async findOne(id: string) {
        try {
            return await this.userRepository.findOneOrFail({
                where: {
                    id,
                }
            })
        } catch (e) {
            if (e instanceof EntityNotFoundError) {
                throw new HttpException(
                    {
                        statusCode: HttpStatus.NOT_FOUND,
                        error: 'Data Not Found',
                    },
                    HttpStatus.NOT_FOUND,
                )
            } else {
                throw e
            }
        }
    }

    async update(id: string, request: UpdateUserDto) {
        try {
            await this.userRepository.findOneOrFail({
                where: {
                    id
                },
                relations: {
                    role: true
                }
            });
        }catch (e) {
            if (e instanceof EntityNotFoundError) {
                throw new HttpException(
                    {
                        statusCode: HttpStatus.NOT_FOUND,
                        error: 'Data Not Found',
                    },
                    HttpStatus.NOT_FOUND,
                )
            } else {
                throw e
            }
        }

        await this.userRepository.update(id, request);
        return this.userRepository.findOneOrFail({
            where: {
                id
            }
        })
    }

    async updatePassword(id: string, request: UpdatePasswordDto) {
        try {
            await this.userRepository.findOneOrFail({
                where: {
                    id,
                }
            })
        } catch (e) {
            if (e instanceof EntityNotFoundError) {
                throw new HttpException(
                    {
                        statusCode: HttpStatus.NOT_FOUND,
                        error: 'Data Not Found',
                    },
                    HttpStatus.NOT_FOUND,
                )
            } else {
                throw e
            }
        }
        const salt = generateString();
        const password = await hashPassword(request.password, salt);
        const data = await this.userRepository.findOneOrFail({
            where: {
                id,
            }
        })
        data.salt = salt;
        data.password = password;

        await this.userRepository.update(id, data);
        return await this.userRepository.findOneOrFail({
            where: {
                id,
            }
        })
    }

    async remove(id: string) {
        try {
            await this.userRepository.findOneOrFail({
                where: {
                    id,
                }
            })
        } catch (e) {
            if (e instanceof EntityNotFoundError) {
                throw new HttpException(
                    {
                        statusCode: HttpStatus.NOT_FOUND,
                        error: 'Data Not Found',
                    },
                    HttpStatus.NOT_FOUND,
                )
            } else {
                throw e
            }
        }
        await this.userRepository.softDelete(id);
    }


}
