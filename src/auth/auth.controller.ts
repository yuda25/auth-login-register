import { Controller, Post, Body, UseGuards, Req, Get } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.sto';
import { RegisterDto } from './dto/register.dto';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService){}

    @Post('/register')
    async register(@Body() request: RegisterDto) {
        await this.authService.register(request);

        return {
            statusCode: 200,
            message: 'success',
        }
    }

    @Post('login')
    async login(@Body() request: LoginDto) {
        const res = await this.authService.login(request);

        return {
            statusCode: 200,
            message: 'success',
            data: res
        }
    }

    @Get()
    @UseGuards(AuthGuard())
    async tesAuth(@Req() req) {
        console.log(req.user);
        // return req.user
        
    }
}
