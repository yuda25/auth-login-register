import { HttpException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { generateString, InjectRepository } from '@nestjs/typeorm';
import { query } from 'express';
import { hashPassword } from 'helper/hash_password';
import { Role } from 'src/users/entities/role.entity';
import { User } from 'src/users/entities/user.entity';
import { IsNull, Repository } from 'typeorm';
import { LoginDto } from './dto/login.sto';
import { RegisterDto } from './dto/register.dto';

@Injectable()
export class AuthService {
    constructor (
        @InjectRepository(User)
        private userRepository: Repository<User>,
        @InjectRepository(Role)
        private roleRepository: Repository<Role>,

        private jwtService: JwtService,
    ) {}

    async register(request: RegisterDto) {
        try {
            const defaultRole = await this.roleRepository.findOneOrFail({
                where: {
                    id: 2,
                }
            })

            const salt = generateString();
            const password = await hashPassword(request.password, salt);
            const data = new User();
            data.nama = request.nama;
            data.email = request.email;
            data.role = defaultRole;
            data.password = password;
            data.salt = salt;
            await this.userRepository.insert(data);
        } catch (e) {
            throw e
        }
    }

    async login(request: LoginDto) {
        try {
            const existUser = await this.userRepository.findOne({
                where: {
                    email: request.email,
                    deletedAt: IsNull(),
                },
                relations: {
                    role: true,
                },
            })
            const { password, salt, ...query } = existUser;

            const accesToken = this.jwtService.sign({
                query,
            });

            if (
                existUser.password ===
                (await hashPassword(request.password, existUser.salt))
            ) {
                return accesToken;
            } else {
                throw new HttpException(
                    {
                        statusCode: 400,
                        messagee: 'Bad Request',
                        data: 'Email or Pasword is wrong',
                    },
                    400,
                );
            }
        } catch (e) {
            throw e
        }
    }
}
