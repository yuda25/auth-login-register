import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { InjectRepository } from "@nestjs/typeorm";
import { ExtractJwt, Strategy } from "passport-jwt";
import { User } from "src/users/entities/user.entity";
import { Repository } from "typeorm";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
    ) {
        super({
            secretOrKey: 'topSecret30',
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        });
    }

    async validate(payload: any) {
        const existUserData =  await this.userRepository.findOne({
            where: {
                email: payload.email
            }
        });

        if (!existUserData) {
            throw new HttpException({
                statusCode: HttpStatus.UNAUTHORIZED,
                message: 'UNAUTHORIZED',
                data: 'Token is invalid'
            },
            HttpStatus.UNAUTHORIZED);
        }
        return existUserData;
    }
}